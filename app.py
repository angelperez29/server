from flask import Flask, render_template, request
import docker
import os

#Variable global
pwd=os.getcwd()

pwd=pwd+'/static/codes'
app = Flask (__name__)
client=docker.from_env()
clientAPI=docker.APIClient()

#Renderizamos la el archivo principal de la app web
@app.route('/')
def index():
    return render_template('index.html')

#Definimos la ruta localhost:5000/code en la que 
#se reciben los datos y el código por medio de 
#POST
@app.route('/code',methods=['POST'])
def returnCode():
    if request.method == 'POST':
        lenguage = request.form['lenguage']
        version = request.form['version']
        code = request.form['code']
        file=writeCode(lenguage,code,version)
        output=executeCode(file,lenguage,version)
    return output

#Función de asignación de tipo de código
def writeCode(lenguage,code,version):
    filename='Main'
    if lenguage == 'python':
        filename=filename+'.py'
        writeFile(filename,code)
    elif (lenguage == 'javascript'):
        filename=filename+'.js'
        writeFile(filename,code)
    elif (lenguage == 'go'):
        filename=filename+'.go'
        writeFile(filename,code)
    elif (lenguage == 'ruby'):
        filename=filename+'.rb'
        writeFile(filename,code)
    elif (lenguage == 'php'):
        filename=filename+'.php'
        writeFile(filename,code)     
    elif (lenguage == 'java'):
        filename=filename+'.java'
        writeFile(filename,code)
    elif (lenguage == 'c'):
        filename=filename+'.c'
        writeFile(filename,code)  
    elif (lenguage == 'cpp'):
        filename=filename+'.cpp'
        writeFile(filename,code) 
    return filename

#Función en la que se ejecuta el código recibido por el usuario
def executeCode(file,lenguage,version):
    output=''
    if lenguage == 'python':
        #                             Imagen:version     Comando
        output=runDocker(f'python:{version}',f'python {file}')
    elif (lenguage == 'javascript'):
        output=runDocker(f'node:{version}',f'node {file}')
    elif (lenguage == 'go'):
        output=runDocker(f'golang:{version}',f'go run {file}')
    elif (lenguage == 'ruby'):
        output=runDocker(f'ruby:{version}',f'ruby {file}')
    elif (lenguage == 'php'):
        output=runDocker(f'php:{version}',f'php {file}')
    elif (lenguage == 'java'):
        output=runDockerCompiled(f'openjdk:{version}',f'javac {file}','java Main')  
    elif (lenguage == 'c'):
        output=runDockerCompiled(f'frolvlad/alpine-gxx',f'gcc {file} -o Main','./Main')
    elif (lenguage == 'cpp'):
        output=runDockerCompiled(f'frolvlad/alpine-gxx',f'g++ {file} -o Main','./Main')
    return output


#Funciones de escritura de código en archivo
#Todos los archivos almacenados en la 
#misma carpeta para tener un poco de orden
def writeFile(filename,code):
    filenameW=pwd+'/'+filename
    file=open(filenameW,'w')
    file.write(code)
    file.close() 

#Ejecución de docker similar a 
#docker run -it --rm -v "$PWD":/usr/src/myapp -w /usr/src/myapp imagen:version lenguaje script-code.extension 
def runDocker(image,comando):
    #Bloque de ejeción de código
    try:
        return client.containers.run(image,network='none',remove=True,stdout=True,
                                    stdin_open=True,stderr=True,
                                    volumes={pwd:{'bind':'/usr/src/myapp'}},command=comando,
                                    working_dir='/usr/src/myapp')            
    except docker.errors.ContainerError as e:
        return str(e)
    
def runDockerCompiled(image,commandC,commandR):
    #Bloque de ejeción de código
    try:
        client.containers.run(image,network='none',remove=True,stdout=True,
                                    stdin_open=True,stderr=True,
                                    volumes={pwd:{'bind':'/usr/src/myapp'}},command=commandC,
                                    working_dir='/usr/src/myapp')
        return client.containers.run(image,network='none',remove=True,stdout=True,
                                    stdin_open=True,stderr=True,
                                    volumes={pwd:{'bind':'/usr/src/myapp'}},command=commandR,
                                    working_dir='/usr/src/myapp')           
    except docker.errors.ContainerError as e:
        return str(e)